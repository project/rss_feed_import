<?php

/*
 * @file
 * Module file for node_generator module.
 */

/**
 * Implements hook_init(). 
 */
function rss_feed_import_init() {
    drupal_add_js(drupal_get_path('module', 'rss_feed_import') . '/rss_feed_import.js');
}

/**
 * Implements hook_menu().
 */
function rss_feed_import_menu() {
    $items['admin/config/system/rss-import'] = array(
        'title' => 'RSS IMPORT',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('rss_feed_import_input_form'),
        'access arguments' => array('access rss_feed_import settings'),
        'type' => MENU_LOCAL_TASK,
        'file' => 'rss_feed_import.admin.inc'
    );
    $items['getfield-rss-import/%'] = array(
        'page callback' => 'get_refrence_fields',
        'type' => MENU_CALLBACK,
        'page arguments' => array(1),
        'access arguments' => array('access content'),
    );
    $items['getfield-rss-voc-import/%'] = array(
        'page callback' => 'rss_get_voc_fields',
        'type' => MENU_CALLBACK,
        'page arguments' => array(1),
        'access arguments' => array('access content'),
    );
    return $items;
}

/**
 * Callback to return JSON encoded for given content type.
 */
function rss_get_voc_fields($type) {
    $fields = rss_get_voc_fields_array($type);
    drupal_json_output($fields);
}

/**
 * Callback to return Term Refrence fields for given content type.
 */
function rss_get_voc_fields_array($data) {
    $fields = array();
    $query = db_select('field_config', 'fc');
    $query->join('field_config_instance', 'fci', 'fc.id = fci.field_id');
    $query->fields('fc', array('field_name'));
    $query->condition('fc.type', 'taxonomy_term_reference');
    $query->condition('fci.bundle', $data);
    $result = $query->execute();
    $record = $result->fetchAll();
    foreach ($record as $key => $value) {
        $tmp = field_info_instance('node', $value->field_name, $data);
        $tmp1 = $tmp['label'];
        $tmp2 = $value->field_name;
        $fields[$tmp2] = $tmp1;
    }
    return $fields;
}

/**
 * Callback to return JSON encoded for given content type.
 */
function get_refrence_fields($type) {
    $fields = rss_import_get_fields($type);
    drupal_json_output($fields);
}

/**
 * Callback to return Term Refrence fields for given content type.
 */
function rss_import_get_fields($data) {
    $get_field_info = field_info_field($data);
    $get_voc = $get_field_info['settings']['allowed_values'][0]['vocabulary'];
    $get_voc_vid = taxonomy_vocabulary_machine_name_load($get_voc);
    $get_all_term = taxonomy_get_tree($get_voc_vid->vid, $parent = 0, $max_depth = NULL, $load_entities = FALSE);
    foreach ($get_all_term as $key => $value) {
        $fields[$value->tid] = $value->name;
    }
    return $fields;
}

/**
 * Implements hook_permission(). 
 */
function rss_feed_import_permission() {
    return array(
        'access rss_feed_import settings' => array(
            'title' => t('Access Rss Import Form'),
            'description' => t('Generate the nodes from external rss url.'),
        ),
    );
}

/**
 * Implements hook_cron(). 
 */
function rss_feed_import_cron() {
    $url = variable_get('rss-import-url', $default = NULL);
    if ($url != NULL) {
        $field = variable_get('rss-import-field', $default = NULL);
        $type = variable_get('rss-import-type', $default = 'page');
        $s_tid = variable_get('rss-import-voc', $default = NULL);
        $counter = variable_get('rss-import-count', $default = 10);
        $tid = unserialize($s_tid);
        $xml = simplexml_load_file($url);
        $count = 0;
        $result = array_unique($tid);
        foreach ($result as $key => $value) {
            $tid_attach['und'][] = array('tid' => $value);
        }
        foreach ($xml as $url) {
            $alias = rss_import_generate_alias($url->link);
            $exist = rss_import_check_alias_exist($alias);
            if ($exist == false) {
                $count++;
                $alias = rss_import_generate_alias($url->link);
                $summary = substr($url->description, 0, 250);
                $node = new stdClass();
                $node->uid = 1;
                $node->type = $type;
                $node->created = time();
                $node->title = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $url->title);
                $node->status = 1;
                $node->promote = 0;
                $node->sticky = 0;
                $node->language = language_default()->language;
                $node->body = array('und' => array(array('value' => $url->description)));
                $node->body['und'][0]['summary'] = $summary;
                $node->body['und'][0]['format'] = 'full_html';
                if ($s_tid != NULL && $field != NULL) {
                    $node->$field = $tid_attach;
                }
                try {
                    node_save($node);
                    $path = array(
                        'source' => 'node/' . $node->nid,
                        'alias' => $alias,
                        'language' => language_default()->language, //Default language of the site.
                    );
                    path_save($path); //Save the path alias.
                } catch (Exception $e) {
                    watchdog('rss-data-import', 'Node Not Created for url %url due to some error.', array('%url' => $url->link)); //If error occurs.
                }
            }
            if ($count == $counter) {
                break;
            }
        }
    }
}

/**
 * Method to generate the alias for the given url node.
 * @param type $url
 * @return string
 */
function rss_import_generate_alias($url) {
    $newurl = str_replace("?ref=rss", "", $url);
    $substr = substr($newurl, strpos($newurl, '//') + 2);
    if (strpos($substr, '/') === FALSE) {  
        return '';
    }
    $alias = str_replace(' ', '-', substr($substr, strpos($substr, '/') + 1)); //Get the url after first / and replace space with -.
    return $alias;
}

/**
 * Method to check whether given alias already exists or not.
 * @param type $alias.
 * @return string.
 */
function rss_import_check_alias_exist($alias) {
    $sql = db_select('url_alias', 'ua');
    $sql->fields('ua', array('pid'));
    $sql->condition('ua.alias', $alias);
    return $sql->execute()->fetchField();
}