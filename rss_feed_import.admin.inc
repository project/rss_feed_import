<?php

/**
 * @file
 * Admin settings file for node_generator module. 
 */

/**
 * Form constructor for rss_feed_import_input_form form.
 */
function rss_feed_import_input_form($form, &$form_state) {
    drupal_set_title(t('RSS IMPORT'));
    $form['rss_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Feed Url'),
        '#required' => TRUE,
        '#description' => t('Absoult Rss Feed Url.'),
    );
    $content_types = node_type_get_names(); //Get all content types list
    foreach ($content_types as $key => $val) {
        if (!rss_import_get_content_types($key, 'body')) { //Check if given content type contains body field or not.
            unset($content_types[$key]); //If not contain body field, remove that content type from list.
        }
    }
    $form['select_type'] = array(
        '#type' => 'select',
        '#options' => array('' => t('Select')) + $content_types,
        '#title' => t('Select Content Type'),
        '#required' => TRUE,
        '#description' => t('Select content type for which you want to create the node.'),
    );
    $form['selectt_category_field'] = array(
        '#type' => 'select', 
        '#options' => array('' => t('Select')),
        '#title' => t('Select Field For Category'),
        '#validated' => TRUE,
        '#required' => false,
        '#description' => t('Select Term Refrence Field for Selected Content Type For which you want to create the category.'),
    );
    
    $form['select_category_field'] = array(
        '#type' => 'select',
        '#size'=> 5,
        '#multiple' => TRUE, 
        '#options' => array('' => t('Select')),
        '#title' => t('Select Field For Category'),
        '#validated' => TRUE,
        '#required' => false,
        '#description' => t('Select Term Refrence Field for Selected Content Type For which you want to create the category.'),
    );
    $form['count_cron'] = array(
        '#type' => 'textfield',
        '#title' => t('Count'),
        '#required' => TRUE,
        '#description' => t('No of Node Import Per Cron.'),
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit')
    );
    return $form;
}


/**
 * Submission handler for node_generator_input_form form.
 * @param type $form
 * @param type $form_state 
 */
function rss_feed_import_input_form_submit($form, &$form_state) {
    $url = $form_state['values']['rss_url'];
    if($form_state['values']['count_cron'] == ''){
    $count = 10;
    }
    else{
    $count = $form_state['values']['count_cron'];
    }
    $type = $form_state['values']['select_type'];
    $voc = $form_state['values']['select_category_field'];
    $s_voc = serialize($voc);
    $field = $form_state['values']['selectt_category_field'];
    if ($form_state['values']['selectt_category_field'] == '') {
        $field = NULL;
    }
    if (empty($form_state['values']['select_category_field'])) {
        $s_voc = NULL;
    }
    variable_set('rss-import-url', $url);
    variable_set('rss-import-type', $type);
    variable_set('rss-import-voc', $s_voc);
    variable_set('rss-import-field', $field);
    variable_set('rss-import-count', $count);
    drupal_set_message(t('Configuration Save.'), 'status');
}



/**
 * Implements hook_validate(). 
 */
function rss_feed_import_input_form_validate($form, &$form_state) {
    $url = valid_url($form_state['values']['rss_url'], $absolute = TRUE);
    if(!$url){
      form_set_error('rss_url', t('Please Enter Valid Feed Url.'));   
    }
}



/**
 * Method to get check whether given content type contains specified field or not.
 * @param type $bundle
 * @param type $field
 * @return string. 
 */
function rss_import_get_content_types($bundle, $field) {
    $sql = db_select('field_config_instance', 'fci');
    $sql->fields('fci', array('field_id'));
    $sql->condition('fci.bundle', $bundle);
    $sql->condition('fci.field_name', $field);
    return $sql->execute()->fetchField();
}