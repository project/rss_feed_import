(function($) {
    Drupal.behaviors.rss_feed_import = {
        'attach': function(context) {
            $('#edit-select-type', context).change(function(e) {
                var th = $(this);     
                $(th).parents("#rss-data-import-input-form").find("#edit-selectt-category-field").html("");      
                $.getJSON(Drupal.settings.basePath+'getfield-rss-voc-import/'+$(this).val(), function(obj) {
                    var optionss="<option selected='selected' value=''>Please select</option>";
                    $.each(obj, function(keyy, valuee){
                        optionss+= '<option value="' + keyy + '">' + valuee + '</option>'; 
                    });
                    $(th).parents("#rss-data-import-input-form").find("#edit-selectt-category-field").html(optionss);
                });
                e.stopImmediatePropagation();
                return false;
            });
            $('#edit-selectt-category-field', context).change(function(e) {
                var th = $(this); 
                $(th).parents("#rss-data-import-input-form").find("#edit-select-category-field").html("");         
                $.getJSON(Drupal.settings.basePath+'getfield-rss-import/'+$(this).val(), function(obj) {
                    var options="<option selected='selected' value=''>Please select</option>";
                    $.each(obj, function(key, value){
                        options+='<option value="' + key + '">' + value + '</option>'; 
                    });
                    $(th).parents("#rss-data-import-input-form").find("#edit-select-category-field").html(options);
                });

                e.stopImmediatePropagation();
                return false;
            });  
        }
    }
})(jQuery);