                                                                     
                                                                     
                                                                     
                                             
-----------------------------------------------------------------
This module import nodes from any Rss feed.


Installation
-------------

* Copy the whole "rss_feed_import" directory to your modules
  directory - "sites/all/modules"
* Enable the module.
* Set your desired bundle wise permissions at - "admin/people/permissions".


Usage
------
   
 * Go to "/admin/config/system/rss-import".
 * Fill required fields in form.

Permissions available
---------------------
* Access Rss Import Form

